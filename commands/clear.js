const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

  if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.reply("Nie możesz usuwać wiadomości na tym kanale!");
  if(!args[0]) return message.channel.send("no");
  message.channel.bulkDelete(args[0]).then(() => {
  message.channel.send(`Usunąłem ${args[0]} wiadomości.`).then(msg => msg.delete(2000));
});

}

module.exports.help = {
  name: "usun"
}
